// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: base.proto

package com.happypay.service.proto;

public final class Base {
  private Base() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface GrpcErrorOrBuilder extends
      // @@protoc_insertion_point(interface_extends:GrpcError)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>.GrpcError.ErrorCode code = 1;</code>
     */
    int getCodeValue();
    /**
     * <code>.GrpcError.ErrorCode code = 1;</code>
     */
    GrpcError.ErrorCode getCode();

    /**
     * <code>string message = 2;</code>
     */
    String getMessage();
    /**
     * <code>string message = 2;</code>
     */
    com.google.protobuf.ByteString
        getMessageBytes();
  }
  /**
   * Protobuf type {@code GrpcError}
   */
  public  static final class GrpcError extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:GrpcError)
      GrpcErrorOrBuilder {
    // Use GrpcError.newBuilder() to construct.
    private GrpcError(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private GrpcError() {
      code_ = 0;
      message_ = "";
    }

    @Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private GrpcError(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 8: {
              int rawValue = input.readEnum();

              code_ = rawValue;
              break;
            }
            case 18: {
              String s = input.readStringRequireUtf8();

              message_ = s;
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return Base.internal_static_GrpcError_descriptor;
    }

    protected FieldAccessorTable
        internalGetFieldAccessorTable() {
      return Base.internal_static_GrpcError_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              GrpcError.class, Builder.class);
    }

    /**
     * Protobuf enum {@code GrpcError.ErrorCode}
     */
    public enum ErrorCode
        implements com.google.protobuf.ProtocolMessageEnum {
      /**
       * <code>CANNOT_CONNECT_TO_HSM = 0;</code>
       */
      CANNOT_CONNECT_TO_HSM(0),
      /**
       * <code>CANNOT_ENCRYPT_TEXT = 1;</code>
       */
      CANNOT_ENCRYPT_TEXT(1),
      /**
       * <code>CANNOT_DECRYPT_TEXT = 2;</code>
       */
      CANNOT_DECRYPT_TEXT(2),
      UNRECOGNIZED(-1),
      ;

      /**
       * <code>CANNOT_CONNECT_TO_HSM = 0;</code>
       */
      public static final int CANNOT_CONNECT_TO_HSM_VALUE = 0;
      /**
       * <code>CANNOT_ENCRYPT_TEXT = 1;</code>
       */
      public static final int CANNOT_ENCRYPT_TEXT_VALUE = 1;
      /**
       * <code>CANNOT_DECRYPT_TEXT = 2;</code>
       */
      public static final int CANNOT_DECRYPT_TEXT_VALUE = 2;


      public final int getNumber() {
        if (this == UNRECOGNIZED) {
          throw new IllegalArgumentException(
              "Can't get the number of an unknown enum value.");
        }
        return value;
      }

      /**
       * @deprecated Use {@link #forNumber(int)} instead.
       */
      @Deprecated
      public static ErrorCode valueOf(int value) {
        return forNumber(value);
      }

      public static ErrorCode forNumber(int value) {
        switch (value) {
          case 0: return CANNOT_CONNECT_TO_HSM;
          case 1: return CANNOT_ENCRYPT_TEXT;
          case 2: return CANNOT_DECRYPT_TEXT;
          default: return null;
        }
      }

      public static com.google.protobuf.Internal.EnumLiteMap<ErrorCode>
          internalGetValueMap() {
        return internalValueMap;
      }
      private static final com.google.protobuf.Internal.EnumLiteMap<
          ErrorCode> internalValueMap =
            new com.google.protobuf.Internal.EnumLiteMap<ErrorCode>() {
              public ErrorCode findValueByNumber(int number) {
                return ErrorCode.forNumber(number);
              }
            };

      public final com.google.protobuf.Descriptors.EnumValueDescriptor
          getValueDescriptor() {
        return getDescriptor().getValues().get(ordinal());
      }
      public final com.google.protobuf.Descriptors.EnumDescriptor
          getDescriptorForType() {
        return getDescriptor();
      }
      public static final com.google.protobuf.Descriptors.EnumDescriptor
          getDescriptor() {
        return GrpcError.getDescriptor().getEnumTypes().get(0);
      }

      private static final ErrorCode[] VALUES = values();

      public static ErrorCode valueOf(
          com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
        if (desc.getType() != getDescriptor()) {
          throw new IllegalArgumentException(
            "EnumValueDescriptor is not for this type.");
        }
        if (desc.getIndex() == -1) {
          return UNRECOGNIZED;
        }
        return VALUES[desc.getIndex()];
      }

      private final int value;

      private ErrorCode(int value) {
        this.value = value;
      }

      // @@protoc_insertion_point(enum_scope:GrpcError.ErrorCode)
    }

    public static final int CODE_FIELD_NUMBER = 1;
    private int code_;
    /**
     * <code>.GrpcError.ErrorCode code = 1;</code>
     */
    public int getCodeValue() {
      return code_;
    }
    /**
     * <code>.GrpcError.ErrorCode code = 1;</code>
     */
    public ErrorCode getCode() {
      ErrorCode result = ErrorCode.valueOf(code_);
      return result == null ? ErrorCode.UNRECOGNIZED : result;
    }

    public static final int MESSAGE_FIELD_NUMBER = 2;
    private volatile Object message_;
    /**
     * <code>string message = 2;</code>
     */
    public String getMessage() {
      Object ref = message_;
      if (ref instanceof String) {
        return (String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        message_ = s;
        return s;
      }
    }
    /**
     * <code>string message = 2;</code>
     */
    public com.google.protobuf.ByteString
        getMessageBytes() {
      Object ref = message_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        message_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (code_ != ErrorCode.CANNOT_CONNECT_TO_HSM.getNumber()) {
        output.writeEnum(1, code_);
      }
      if (!getMessageBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 2, message_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (code_ != ErrorCode.CANNOT_CONNECT_TO_HSM.getNumber()) {
        size += com.google.protobuf.CodedOutputStream
          .computeEnumSize(1, code_);
      }
      if (!getMessageBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, message_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @Override
    public boolean equals(final Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof GrpcError)) {
        return super.equals(obj);
      }
      GrpcError other = (GrpcError) obj;

      boolean result = true;
      result = result && code_ == other.code_;
      result = result && getMessage()
          .equals(other.getMessage());
      return result;
    }

    @Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + CODE_FIELD_NUMBER;
      hash = (53 * hash) + code_;
      hash = (37 * hash) + MESSAGE_FIELD_NUMBER;
      hash = (53 * hash) + getMessage().hashCode();
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static GrpcError parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static GrpcError parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static GrpcError parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static GrpcError parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static GrpcError parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static GrpcError parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static GrpcError parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static GrpcError parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static GrpcError parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static GrpcError parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(GrpcError prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @Override
    protected Builder newBuilderForType(
        BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code GrpcError}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:GrpcError)
        GrpcErrorOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return Base.internal_static_GrpcError_descriptor;
      }

      protected FieldAccessorTable
          internalGetFieldAccessorTable() {
        return Base.internal_static_GrpcError_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                GrpcError.class, Builder.class);
      }

      // Construct using com.happypay.service.proto.Base.GrpcError.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        code_ = 0;

        message_ = "";

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return Base.internal_static_GrpcError_descriptor;
      }

      public GrpcError getDefaultInstanceForType() {
        return GrpcError.getDefaultInstance();
      }

      public GrpcError build() {
        GrpcError result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public GrpcError buildPartial() {
        GrpcError result = new GrpcError(this);
        result.code_ = code_;
        result.message_ = message_;
        onBuilt();
        return result;
      }

      public Builder clone() {
        return (Builder) super.clone();
      }
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.setField(field, value);
      }
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return (Builder) super.clearField(field);
      }
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return (Builder) super.clearOneof(oneof);
      }
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, Object value) {
        return (Builder) super.setRepeatedField(field, index, value);
      }
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.addRepeatedField(field, value);
      }
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof GrpcError) {
          return mergeFrom((GrpcError)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(GrpcError other) {
        if (other == GrpcError.getDefaultInstance()) return this;
        if (other.code_ != 0) {
          setCodeValue(other.getCodeValue());
        }
        if (!other.getMessage().isEmpty()) {
          message_ = other.message_;
          onChanged();
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        GrpcError parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (GrpcError) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private int code_ = 0;
      /**
       * <code>.GrpcError.ErrorCode code = 1;</code>
       */
      public int getCodeValue() {
        return code_;
      }
      /**
       * <code>.GrpcError.ErrorCode code = 1;</code>
       */
      public Builder setCodeValue(int value) {
        code_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>.GrpcError.ErrorCode code = 1;</code>
       */
      public ErrorCode getCode() {
        ErrorCode result = ErrorCode.valueOf(code_);
        return result == null ? ErrorCode.UNRECOGNIZED : result;
      }
      /**
       * <code>.GrpcError.ErrorCode code = 1;</code>
       */
      public Builder setCode(ErrorCode value) {
        if (value == null) {
          throw new NullPointerException();
        }
        
        code_ = value.getNumber();
        onChanged();
        return this;
      }
      /**
       * <code>.GrpcError.ErrorCode code = 1;</code>
       */
      public Builder clearCode() {
        
        code_ = 0;
        onChanged();
        return this;
      }

      private Object message_ = "";
      /**
       * <code>string message = 2;</code>
       */
      public String getMessage() {
        Object ref = message_;
        if (!(ref instanceof String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          String s = bs.toStringUtf8();
          message_ = s;
          return s;
        } else {
          return (String) ref;
        }
      }
      /**
       * <code>string message = 2;</code>
       */
      public com.google.protobuf.ByteString
          getMessageBytes() {
        Object ref = message_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (String) ref);
          message_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>string message = 2;</code>
       */
      public Builder setMessage(
          String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        message_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>string message = 2;</code>
       */
      public Builder clearMessage() {
        
        message_ = getDefaultInstance().getMessage();
        onChanged();
        return this;
      }
      /**
       * <code>string message = 2;</code>
       */
      public Builder setMessageBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        message_ = value;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:GrpcError)
    }

    // @@protoc_insertion_point(class_scope:GrpcError)
    private static final GrpcError DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new GrpcError();
    }

    public static GrpcError getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<GrpcError>
        PARSER = new com.google.protobuf.AbstractParser<GrpcError>() {
      public GrpcError parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new GrpcError(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<GrpcError> parser() {
      return PARSER;
    }

    @Override
    public com.google.protobuf.Parser<GrpcError> getParserForType() {
      return PARSER;
    }

    public GrpcError getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_GrpcError_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_GrpcError_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\nbase.proto\"\232\001\n\tGrpcError\022\"\n\004code\030\001 \001(\016" +
      "2\024.GrpcError.ErrorCode\022\017\n\007message\030\002 \001(\t\"" +
      "X\n\tErrorCode\022\031\n\025CANNOT_CONNECT_TO_HSM\020\000\022" +
      "\027\n\023CANNOT_ENCRYPT_TEXT\020\001\022\027\n\023CANNOT_DECRY" +
      "PT_TEXT\020\002B\034\n\032com.happypay.service.protob" +
      "\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_GrpcError_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_GrpcError_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_GrpcError_descriptor,
        new String[] { "Code", "Message", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
