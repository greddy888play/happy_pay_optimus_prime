package com.happypay.dataencryption.grpc;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.3.0)",
    comments = "Source: data_encryption.proto")
public final class DataEncryptionServiceGrpc {

  private DataEncryptionServiceGrpc() {}

  public static final String SERVICE_NAME = "DataEncryptionService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.happypay.dataencryption.grpc.EncryptPlainTextCommand,
      com.happypay.dataencryption.grpc.EncryptPlainTextResult> METHOD_ENCRYPT =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "DataEncryptionService", "encrypt"),
          io.grpc.protobuf.ProtoUtils.marshaller(com.happypay.dataencryption.grpc.EncryptPlainTextCommand.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(com.happypay.dataencryption.grpc.EncryptPlainTextResult.getDefaultInstance()));
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.happypay.dataencryption.grpc.DecryptCipherTextCommand,
      com.happypay.dataencryption.grpc.DecryptCipherTextResult> METHOD_DECRYPT =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "DataEncryptionService", "decrypt"),
          io.grpc.protobuf.ProtoUtils.marshaller(com.happypay.dataencryption.grpc.DecryptCipherTextCommand.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(com.happypay.dataencryption.grpc.DecryptCipherTextResult.getDefaultInstance()));

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DataEncryptionServiceStub newStub(io.grpc.Channel channel) {
    return new DataEncryptionServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DataEncryptionServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DataEncryptionServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary and streaming output calls on the service
   */
  public static DataEncryptionServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DataEncryptionServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class DataEncryptionServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void encrypt(com.happypay.dataencryption.grpc.EncryptPlainTextCommand request,
        io.grpc.stub.StreamObserver<com.happypay.dataencryption.grpc.EncryptPlainTextResult> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_ENCRYPT, responseObserver);
    }

    /**
     */
    public void decrypt(com.happypay.dataencryption.grpc.DecryptCipherTextCommand request,
        io.grpc.stub.StreamObserver<com.happypay.dataencryption.grpc.DecryptCipherTextResult> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_DECRYPT, responseObserver);
    }

    @Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_ENCRYPT,
            asyncUnaryCall(
              new MethodHandlers<
                com.happypay.dataencryption.grpc.EncryptPlainTextCommand,
                com.happypay.dataencryption.grpc.EncryptPlainTextResult>(
                  this, METHODID_ENCRYPT)))
          .addMethod(
            METHOD_DECRYPT,
            asyncUnaryCall(
              new MethodHandlers<
                com.happypay.dataencryption.grpc.DecryptCipherTextCommand,
                com.happypay.dataencryption.grpc.DecryptCipherTextResult>(
                  this, METHODID_DECRYPT)))
          .build();
    }
  }

  /**
   */
  public static final class DataEncryptionServiceStub extends io.grpc.stub.AbstractStub<DataEncryptionServiceStub> {
    private DataEncryptionServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DataEncryptionServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected DataEncryptionServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DataEncryptionServiceStub(channel, callOptions);
    }

    /**
     */
    public void encrypt(com.happypay.dataencryption.grpc.EncryptPlainTextCommand request,
        io.grpc.stub.StreamObserver<com.happypay.dataencryption.grpc.EncryptPlainTextResult> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_ENCRYPT, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void decrypt(com.happypay.dataencryption.grpc.DecryptCipherTextCommand request,
        io.grpc.stub.StreamObserver<com.happypay.dataencryption.grpc.DecryptCipherTextResult> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_DECRYPT, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class DataEncryptionServiceBlockingStub extends io.grpc.stub.AbstractStub<DataEncryptionServiceBlockingStub> {
    private DataEncryptionServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DataEncryptionServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected DataEncryptionServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DataEncryptionServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.happypay.dataencryption.grpc.EncryptPlainTextResult encrypt(com.happypay.dataencryption.grpc.EncryptPlainTextCommand request) {
      return blockingUnaryCall(
          getChannel(), METHOD_ENCRYPT, getCallOptions(), request);
    }

    /**
     */
    public com.happypay.dataencryption.grpc.DecryptCipherTextResult decrypt(com.happypay.dataencryption.grpc.DecryptCipherTextCommand request) {
      return blockingUnaryCall(
          getChannel(), METHOD_DECRYPT, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class DataEncryptionServiceFutureStub extends io.grpc.stub.AbstractStub<DataEncryptionServiceFutureStub> {
    private DataEncryptionServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DataEncryptionServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected DataEncryptionServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DataEncryptionServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.happypay.dataencryption.grpc.EncryptPlainTextResult> encrypt(
        com.happypay.dataencryption.grpc.EncryptPlainTextCommand request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_ENCRYPT, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.happypay.dataencryption.grpc.DecryptCipherTextResult> decrypt(
        com.happypay.dataencryption.grpc.DecryptCipherTextCommand request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_DECRYPT, getCallOptions()), request);
    }
  }

  private static final int METHODID_ENCRYPT = 0;
  private static final int METHODID_DECRYPT = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DataEncryptionServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DataEncryptionServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ENCRYPT:
          serviceImpl.encrypt((com.happypay.dataencryption.grpc.EncryptPlainTextCommand) request,
              (io.grpc.stub.StreamObserver<com.happypay.dataencryption.grpc.EncryptPlainTextResult>) responseObserver);
          break;
        case METHODID_DECRYPT:
          serviceImpl.decrypt((com.happypay.dataencryption.grpc.DecryptCipherTextCommand) request,
              (io.grpc.stub.StreamObserver<com.happypay.dataencryption.grpc.DecryptCipherTextResult>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @Override
    @SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class DataEncryptionServiceDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.happypay.dataencryption.grpc.DataEncryption.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DataEncryptionServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DataEncryptionServiceDescriptorSupplier())
              .addMethod(METHOD_ENCRYPT)
              .addMethod(METHOD_DECRYPT)
              .build();
        }
      }
    }
    return result;
  }
}
