package com.happypay.optimus;

import com.happypay.dataencryption.grpc.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.junit.Test;

/**
 * Created by HappyPay on 20/07/2017.
 */
public class GrpcClientTest {
    private String host = "localhost";
    private int port = 6565;
    private ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext(true).build();

    private String plaintext = "Hello Happy Pay!! 你好，支付樂！！";
    private String ciphertext = "nftV+m8nYkF/I7PTN2xPwonpic9G9peXo9c6Da48+yF9bWcKpfoTg4UG0QBwGp35sYpKvAxH+DfpZYjbdYDMzg==";

    @Test
    public void testDataEncryption () {
        DataEncryptionServiceGrpc.DataEncryptionServiceBlockingStub stub = DataEncryptionServiceGrpc.newBlockingStub(channel);
        EncryptPlainTextResult result = stub.encrypt(EncryptPlainTextCommand.newBuilder().setPlainText(plaintext).build());
        ciphertext = result.getCipherText();
        System.out.println(result.getCipherText());
    }

    @Test
    public void testDataDecryption () {
        DataEncryptionServiceGrpc.DataEncryptionServiceBlockingStub stub = DataEncryptionServiceGrpc.newBlockingStub(channel);
        DecryptCipherTextResult result = stub.decrypt(DecryptCipherTextCommand.newBuilder().setCipherText(ciphertext).build());
        System.out.println(result.getPlainText());
    }
}
