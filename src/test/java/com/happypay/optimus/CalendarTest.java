package com.happypay.optimus;

import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by HappyPay on 18/07/2017.
 */
public class CalendarTest {

    @Test
    public void testTimeZone() {
        Calendar calendar = Calendar.getInstance();

        DateFormat formatter= new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
        System.out.println(formatter.format(calendar.getTime()));

        System.out.println(calendar.getTimeZone());
        System.out.println(new Date(calendar.getTimeInMillis()));
    }
}
